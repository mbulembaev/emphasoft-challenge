import json
from typing import List
from loaders import IrsLoader


def load_form_data(forms: List[str]):
    """Load form data for given forms"""
    results = []

    for form in forms:
        loader = IrsLoader(form)
        data = loader.get_data()
        results.append(data)

    return json.dumps(results)


if __name__ == '__main__':
    product_numbers = input(
        'Enter exact Product numbers separated by commas (ex: Form W-2, Form 1095-C): '
    )
    product_numbers = [product.strip() for product in product_numbers.split(',')]
    print(load_form_data(product_numbers))
