# Emphasoft test challenge
Written on Python 3.8.5, BeautifulSoup used.

### Installing on a local machine

Clone repository

```bash
git clone "git@gitlab.com:mbulembaev/emphasoft-challenge.git"
cd "emphasoft-challenge"
```

Create Virtual environment, then activate it

```bash
python -m venv venv
source venv/bin/activate
```

Project requires pip-tools. To install all requirements:

```bash
(venv)$ python -m pip install pip-tools
(venv)$ make
```

### How To Run

- Run `python irs_form_data.py`

  1.  Enter an exact match tax form name (ex: Form W-2) or also user can enter multiple tax form names separated by
      a comma (ex: Form W-2, Form 1095-C)
  

- Run `python irs_form_pdf.py`

  1.  Enter an exact match tax form name (ex: Form W-2)
  2.  Then, enter year (ex: 2018) or range of years (ex: 2018-2021) to download all PDFs available within that year
      or range of years

      - `2018` will download only one file for given tax form for that year
      - `2018-2021` will download given tax form with revision dates `2018`, `2019`, `2020`, `2021`

  3.  Files will be downloaded to local folder in project directory `./emphasoft-challenge/files/{form_name}/`



- Testing

```bash
$ pytest
```
