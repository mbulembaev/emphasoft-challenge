from loaders import IrsLoader


def load_form_files(form: str, years: str):
    """Download forms files for a given range of years"""

    range_years = years.split("-")
    if len(range_years) == 1:
        year = int(years.strip())
        min_year = year
        max_year = year
    else:
        min_year, max_year = [int(year.strip()) for year in range_years]

    if min_year > max_year:
        raise ValueError('Second year must be greater than first')

    loader = IrsLoader(form)
    loader.download_tax_form(min_year, max_year)


if __name__ == '__main__':
    product_number = input('Enter exact Product number (ex: Form W-2): ')
    year_range = input(
        'Enter year or range of years to download all PDFs available within'
        ' that year or range of years (ex: 2018 or 2018-2020): '
    )
    load_form_files(product_number, year_range)
