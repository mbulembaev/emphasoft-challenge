import os
from urllib import request
import requests
from bs4 import BeautifulSoup, ResultSet


class IrsLoader:
    def __init__(self, form_name: str):
        self.form_name = form_name
        self.form_value = form_name.replace(' ', '+')
        self.current_page = 1
        self.base_url = 'https://apps.irs.gov/app/picklist/list/priorFormPublication.html?'
        self.limit = 200
        self.bs = None

    def get_search_result(self) -> ResultSet:
        """Get search result"""
        table = self.bs.find('table', {'class': 'picklist-dataTable'})
        results = table.find_all('tr')[1:]
        return results

    def send_query(self):
        """Send query with current form_name and page
        and get data for bs4"""
        first_row_index = (self.current_page - 1) * self.limit
        url = f'{self.base_url}resultsPerPage={self.limit}' \
              f'&sortColumn=sortOrder&indexOfFirstRow={first_row_index}' \
              f'&criteria=formNumber&value={self.form_value}&isDescending=false'
        response = requests.get(url)
        self.bs = BeautifulSoup(response.text, 'html.parser')

    def get_total_pages(self) -> int:
        """Get total number of pages"""
        pagination = self.bs.find('div', {'class': 'paginationBottom'})
        if not pagination:
            return 0

        pages = pagination.findAll('a')
        pagination_current = pagination.find('b')
        current_page = int(pagination_current.string)

        for page in range(len(pages) - 1, -1, -1):
            index_page_string = pages[page].string
            if index_page_string.isnumeric():
                index_page = int(index_page_string)
                return max(index_page, current_page)
        return current_page

    def get_data(self) -> dict:
        """Get data for a given form"""
        self.send_query()
        total_pages = self.get_total_pages()
        form_title = None
        min_year = None
        max_year = None

        while self.current_page <= total_pages:
            for row in self.get_search_result():
                product_number_cell, title_cell, revision_date_cell = row.find_all('td')

                product_number = product_number_cell.find('a').string
                if product_number == self.form_name:

                    form_title = title_cell.string.strip()

                    revision_date = int(revision_date_cell.string.strip())
                    if not min_year or revision_date < min_year:
                        min_year = revision_date
                    if not max_year or revision_date > max_year:
                        max_year = revision_date

            self.current_page += 1
            self.send_query()

        return {
            'form_number': self.form_name,
            'form_title': form_title,
            'min_year': min_year,
            'max_year': max_year,
        }

    def download_tax_form(self, min_year: int, max_year: int):
        self.send_query()
        total_pages = self.get_total_pages()

        while self.current_page <= total_pages:
            for row in self.get_search_result():
                product_number_cell, title_cell, revision_date_cell = row.find_all('td')

                product_number = product_number_cell.find('a').string
                if product_number != self.form_name:
                    continue

                revision_date = int(revision_date_cell.string.strip())
                if min_year <= revision_date <= max_year:
                    file_url = product_number_cell.find('a')['href']
                    path = f'files/{self.form_name}'
                    filename = f'{self.form_name} - {revision_date}.pdf'

                    if not os.path.isdir(path):
                        os.makedirs(path)

                    if os.path.isfile(f'{path}/{filename}'):
                        print(f'{filename} already exists')
                        continue

                    request.urlretrieve(file_url, f'./{path}/{filename}')
                    print(f'{filename} downloaded')

            self.current_page += 1
            self.send_query()
