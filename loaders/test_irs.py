from bs4 import BeautifulSoup
from loaders import IrsLoader

form_name = 'Form W-2'
form_value = 'Form+W-2'
number_of_pages = 2
data = {
    "form_number": "Form W-2",
    "form_title": "Wage and Tax Statement (Info Copy Only)",
    "min_year": 1954,
    "max_year": 2021
}
path = f'files/{form_name}'
filename_2018 = f'{form_name} - 2018.pdf'
filename_2019 = f'{form_name} - 2019.pdf'

loader = IrsLoader(form_name)
loader.send_query()


class TestIrsLoader:
    def test_search_result(self):
        rows = loader.get_search_result()
        product_number_cell, title_cell, revision_date_cell = rows[23].find_all('td')
        assert len(rows[0].findAll("td")) == 3
        assert product_number_cell.find('a').string == data['form_number']
        assert title_cell.string.strip() == data['form_title']
        assert type(int(revision_date_cell.string.strip())) == int

    def test_total_pages(self):
        total_pages = loader.get_total_pages()
        assert total_pages == number_of_pages

    def test_get_data(self):
        loader_data = loader.get_data()
        assert loader_data == data

    def test_form_name(self):
        assert loader.form_name == form_name

    def test_form_value(self):
        assert loader.form_value == form_value

    def test_bs(self):
        assert type(loader.bs) == BeautifulSoup
